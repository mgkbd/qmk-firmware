// Copyright 2023 QMK
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     * ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╗
     * ║Esc│ 1 │ 2 │ 3 │ 4 │ 5 │Prt║
     * ╟───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───╢
     * ║ Tab │ Q │ W │ E │ R │Enter║
     * ╟─────┴┬──┴┬──┴┬──┴┬──┴┐    ║
     * ║Shift │ A │ S │ D │ F │    ║
     * ╟───┬──┴┬──┴┬──┴┬──┴───┴┬───╢
     * ║Ctl│Fn │ C │ V │ Space │Mut║
     * ╚═══╧═══╧═══╧═══╧═══════╧═══╝
     * default layer
     */
    [0] = LAYOUT(
        KC_ESC,  KC_1,  KC_2, KC_3, KC_4,   KC_5,   KC_PSCR,
        KC_TAB,  KC_Q,  KC_W, KC_E, KC_R,   KC_ENT,
        KC_LSFT, KC_A,  KC_S, KC_D, KC_F,
        KC_LCTL, MO(1), KC_C, KC_V, KC_SPC, KC_MUTE
    ),
    /*
     * ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╗
     * ║   │ 6 │ 7 │ 8 │ 9 │ 0 │Ply║
     * ╟───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───╢
     * ║     │Hu+│Sa+│Br+│Sp+│OnOff║
     * ╟─────┴┬──┴┬──┴┬──┴┬──┴┐    ║
     * ║      │Hu-│Sa-│Br-│Sp-│    ║
     * ╟───┬──┴┬──┴┬──┴┬──┴───┴┬───╢
     * ║   │▒▒▒│MdR│Mod│       │   ║
     * ╚═══╧═══╧═══╧═══╧═══════╧═══╝
     * function layer
     */
    [1] = LAYOUT(
        _______, KC_6,    KC_7,     KC_8,    KC_9,    KC_0,    KC_MPLY,
        _______, RGB_HUI, RGB_SAI,  RGB_VAI, RGB_SPI, RGB_TOG,
        _______, RGB_HUD, RGB_SAD,  RGB_VAD, RGB_SPD,
        _______, _______, RGB_RMOD, RGB_MOD, _______, _______
    )
};
