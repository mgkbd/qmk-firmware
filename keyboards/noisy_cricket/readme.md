# noisy cricket (mini gaming keyboard)

<<<<<<< HEAD
![front](https://imgur.com/sgD8X4e.png)
=======
![front](https://imgur.com/sgD8X4e.png) 
>>>>>>> f5cd3ad8684b08c2e98cbed3f8291ec55d6fcd1d
![back](https://imgur.com/6gjiKvC.png)

A QMK based mini gaming keyboard intended gaming on the go.

<<<<<<< HEAD
* Keyboard Maintainer: [XenGi](https://gitlab.com/XenGi)
* Hardware Supported: [PCB](https://gitlab.com/noisy_cricket/pcb)
* Hardware Availability: [gerber files](https://gitlab.com/noisy_cricket/pcb/-/releases/rev2)
* USB ID: [1209:4237](https://pid.codes/1209/4237/)
=======
* Keyboard Maintainer: [XenGi](https://github.com/XenGi)
* Hardware Supported: [t9](https://github.com/noisy_cricket/pcb)
* Hardware Availability: [gerber files](https://gitlab.com/noisy_cricket/pcb/-/releases/rev1)
>>>>>>> f5cd3ad8684b08c2e98cbed3f8291ec55d6fcd1d

Make example for this keyboard (after setting up your build environment):

```shell
make noisy_cricket:default
# or with the QMK tool
qmk compile -kb noisy_cricket -km default
```

Flashing example for this keyboard:

If you have a fresh ATMEGA chip, you have to erase it before you flash it:

```shell
dfu-programmer atmega32u4 erase --force
```

Afterwards you can do this:

```shell
make noisy_cricket:default:flash
# or with the QMK tool
qmk flash -kb noisy_cricket -km default
```

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Bootloader

Enter the bootloader in 3 ways:

* **Bootmagic reset**: Hold down the key at (0,0) in the matrix (usually the top left key or Escape) and plug in the keyboard
<<<<<<< HEAD
* **Physical reset button**: Briefly press the reset button on the back of the PCB
=======
* **Physical reset button**: Briefly short the pads on the back of the PCB
>>>>>>> f5cd3ad8684b08c2e98cbed3f8291ec55d6fcd1d

